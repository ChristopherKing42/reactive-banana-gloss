module Reactive.Banana.Gloss (
    play,
    playIO,
    playIO',
    module Reactive.Banana.Gloss.Combinators,
    module Graphics.Gloss.Interface.IO.Game,
    compile,
    actuate,
    pause
    ) where

import Control.Concurrent.MVar
import Data.IORef
import Data.Set
import Graphics.Gloss.Interface.IO.Game hiding (Event, playIO)
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import Reactive.Banana.Frameworks
import Reactive.Banana.Gloss.Combinators
import Reactive.Banana.Gloss.MomentGloss

play ::
       Display
    -> Color
    -> Int
    -> MomentGloss (Behavior Picture)
    -> IO ()

play display color framerate react = playIO display color framerate $ liftMomentGloss react

playIO ::
       Display
    -> Color
    -> Int
    -> MomentGlossIO (Behavior Picture)
    -> IO ()

playIO display color framerate react = do
    actVar <- newEmptyMVar
    let network = playIO' display color framerate ((,) () <$> react)  >>= liftIO . putMVar actVar . snd
    en <- compile network
    act <- takeMVar actVar
    actuate en
    act

playIO' ::
        Display
     -> Color
     -> Int
     -> MomentGlossIO (a, Behavior Picture)
     -> MomentIO (a, IO ())

playIO' display color framerate react = do
    (_event, pushEvent) <- newEvent
    (_tick, pushTick) <- newEvent
    (a, picB) <- runMomentGlossIO react =<< createEnv _event _tick display
    pic <- valueB picB
    picV <- liftIO $ newIORef pic
    reactimate' =<< (changes $ writeIORef picV <$> picB)
    return (a, Gloss.playIO display color framerate ()
        (\() -> readIORef picV)
        (\e () -> pushEvent e)
        (\t () -> pushTick t))
