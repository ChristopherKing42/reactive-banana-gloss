{-# LANGUAGE FlexibleContexts, GeneralizedNewtypeDeriving, LambdaCase #-}

module Reactive.Banana.Gloss.MomentGloss (
    Moment,
    MomentIO,
    Behavior,
    MonadMoment(..),
    module Control.Monad.IO.Class,
    MomentGlossEnv(..),
    MomentGloss(..),
    momentGloss,
    runMomentGloss,
    MonadMomentGloss(..),
    MomentGlossIO(..),
    momentGlossIO,
    runMomentGlossIO,
    liftMomentIO,
    event,
    tick,
    keyStore,
    modifiers,
    mousePos,
    windowSize,
    observeEG,
    executeG,
    createEnv
) where

import Control.Monad.Fix
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Functor
import Data.Set
import Graphics.Gloss.Interface.Environment
import Graphics.Gloss.Interface.IO.Game hiding (Event)
import qualified Graphics.Gloss.Interface.IO.Game as Gloss (Event)
import Reactive.Banana.Combinators hiding (Event)
import Reactive.Banana.Frameworks
import qualified Reactive.Banana as Banana (Event)

data MomentGlossEnv = MomentGlossEnv {
    _event :: Banana.Event Gloss.Event,
    _tick :: Banana.Event Float,
    _keyStore :: Behavior (Set Key),
    _modifiers :: Behavior Modifiers,
    _mousePos :: Behavior (Float, Float),
    _windowSize :: Behavior (Int, Int)
    }

newtype MomentGloss a = MomentGloss {_runMomentGloss :: ReaderT MomentGlossEnv Moment a}
    deriving (Functor, Applicative, Monad, MonadFix, MonadReader MomentGlossEnv)

momentGloss = MomentGloss . ReaderT
runMomentGloss = runReaderT . _runMomentGloss

class (MonadMoment m, MonadReader MomentGlossEnv m) => MonadMomentGloss m where
    liftMomentGloss :: MomentGloss a -> m a
    liftMomentGloss (MomentGloss r) = ask >>= liftMoment . runReaderT r

instance MonadMoment MomentGloss where
    liftMoment = MomentGloss . lift

instance MonadMomentGloss MomentGloss

newtype MomentGlossIO a = MomentGlossIO {_runMomentGlossIO :: ReaderT MomentGlossEnv MomentIO a}
    deriving (Functor, Applicative, Monad, MonadFix, MonadIO, MonadReader MomentGlossEnv)

momentGlossIO = MomentGloss . ReaderT
runMomentGlossIO = runReaderT . _runMomentGlossIO

instance MonadMoment MomentGlossIO where
    liftMoment = MomentGlossIO . lift . liftMoment

instance MonadMomentGloss MomentGlossIO

event :: MonadMomentGloss m => m (Banana.Event Gloss.Event)
event = liftMomentGloss $ MomentGloss $ asks _event

tick :: MonadMomentGloss m => m (Banana.Event Float)
tick = liftMomentGloss $ MomentGloss $ asks _tick

keyStore :: MonadMomentGloss m => m (Behavior (Set Key))
keyStore = liftMomentGloss $ MomentGloss $ asks _keyStore

modifiers :: MonadMomentGloss m => m (Behavior Modifiers)
modifiers = liftMomentGloss $ MomentGloss $ asks _modifiers

mousePos :: MonadMomentGloss m => m (Behavior (Float, Float))
mousePos = liftMomentGloss $ MomentGloss $ asks _mousePos

windowSize :: MonadMomentGloss m => m (Behavior (Int, Int))
windowSize = liftMomentGloss $ MomentGloss $ asks _windowSize

liftMomentIO :: MomentIO a -> MomentGlossIO a
liftMomentIO = MomentGlossIO . lift

observeEG :: MonadMomentGloss m => Banana.Event (MomentGloss a) -> m (Banana.Event a)
observeEG e = do
    env <- ask
    let e' = ($ env) . runMomentGloss <$> e
    return $ observeE e'

executeG :: Banana.Event (MomentGlossIO a) -> MomentGlossIO (Banana.Event a)
executeG e = do
    env <- ask
    let e' = ($ env) . runMomentGlossIO <$> e
    liftMomentIO $ execute e'

createEnv _event _tick display = do
    _keyStore <- accumB Data.Set.empty $ filterJust $ _event <&> \case
        (EventKey k Down _ _) -> Just $ insert k
        (EventKey k Up _ _) -> Just $ delete k
        _ -> Nothing
    _modifiers <- stepper (Modifiers Up Up Up) $ filterJust $ _event <&> \case
        (EventKey _ _ mod _) -> Just mod
        _ -> Nothing
    _mousePos <- stepper (0,0) $ filterJust $ _event <&> \case
        (EventKey _ _ _ pos) -> Just pos
        (EventMotion pos) -> Just pos
        _ -> Nothing
    _windowSize0 <- case display of
        (InWindow _ size _) -> return size
        FullScreen -> liftIO $ getScreenSize
    _windowSize <- stepper _windowSize0 $ filterJust $ _event <&> \case
        (EventResize size) -> Just size
        _ -> Nothing
    
    return $ MomentGlossEnv _event _tick _keyStore _modifiers _mousePos _windowSize
