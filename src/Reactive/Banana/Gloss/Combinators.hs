{-# LANGUAGE LambdaCase #-}

module Reactive.Banana.Gloss.Combinators(
    module Reactive.Banana.Gloss.Combinators,
    MomentGloss,
    MomentGlossIO,
    event,
    tick,
    keyStore,
    modifiers,
    mousePos,
    observeEG,
    executeG,
    module Control.Monad.IO.Class,
    liftMomentIO,
    module B
    ) where

import Control.Monad.IO.Class
import Data.Functor
import Data.Set
import Graphics.Gloss.Interface.IO.Game hiding (Event)
import Reactive.Banana.Combinators as B hiding (Event)
import Reactive.Banana.Combinators
import Reactive.Banana.Gloss.MomentGloss

refreshE :: MonadMomentGloss m => m (Event ())
refreshE = do
    e <- event
    t <- tick
    return $ void e <> void t

keyE :: MonadMomentGloss m => m (Key -> KeyState -> Event (Float, Float))
keyE = event <&> \e k ks -> filterJust $ e <&> \case
    (EventKey k' ks' _ pos) | (k', ks') == (k, ks) -> Just pos
    _ -> Nothing

pressedB :: MonadMomentGloss m => m (Key -> Behavior Bool)
pressedB = keyStore <&> \ks k -> (member k) <$> ks

mousePosE :: MonadMomentGloss m => m (Event (Float, Float))
mousePosE = event <&> \e -> filterJust $ e <&> \case
        (EventKey _ _ _ pos) -> Just pos
        (EventMotion pos) -> Just pos
        _ -> Nothing

windowSizeE :: MonadMomentGloss m => m (Event (Int, Int))
windowSizeE = do
    e <- event
    return $ filterJust $ e <&> \case
        (EventResize size) -> Just size
        _ -> Nothing
