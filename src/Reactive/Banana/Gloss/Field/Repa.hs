module Reactive.Banana.Gloss.Field.Repa (
    play,
    playIO,
    playIO',
    module Reactive.Banana.Gloss.Combinators,
    module Graphics.Gloss.Interface.IO.Game,
    module Graphics.Gloss.Data.Color,
    compile,
    actuate,
    pause
    ) where

import Control.Concurrent.MVar
import Data.Array.Repa
import Data.IORef
import Data.Set
import Graphics.Gloss.Data.Color
import Graphics.Gloss.Interface.IO.Game hiding (Event, playIO)
import Graphics.Gloss.Raster.Field
import Reactive.Banana.Frameworks
import Reactive.Banana.Gloss.Combinators
import Reactive.Banana.Gloss.MomentGloss

play ::
       Display
    -> (Int, Int)
    -> Int
    -> MomentGloss (Behavior (Point -> Color))
    -> IO ()
play display resolution framerate react = playIO display resolution framerate $ liftMomentGloss react

playIO ::
       Display
    -> (Int, Int)
    -> Int
    -> MomentGlossIO (Behavior (Point -> Color))
    -> IO ()
playIO display resolution framerate react = do
    actVar <- newEmptyMVar
    let network = playIO' display resolution framerate ((,) () <$> react)  >>= liftIO . putMVar actVar . snd
    en <- compile network
    act <- takeMVar actVar
    actuate en
    act

playIO' ::
       Display
    -> (Int, Int)
    -> Int
    -> MomentGlossIO (a, Behavior (Point -> Color))
    -> MomentIO (a, IO ())
playIO' display resolution framerate react = do
    (_event, pushEvent) <- newEvent
    (_tick, pushTick) <- newEvent
    (a, picB) <- runMomentGlossIO react =<< createEnv _event _tick display
    pic <- valueB picB
    picV <- liftIO $ newIORef pic
    reactimate' =<< (changes $ writeIORef picV <$> picB)
    return (a, playFieldIO display resolution framerate ()
        (\() -> readIORef picV)
        (\e () -> pushEvent e)
        (\t () -> pushTick t))
