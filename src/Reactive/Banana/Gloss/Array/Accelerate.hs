{-# LANGUAGE Rank2Types #-}

module Reactive.Banana.Gloss.Array.Accelerate (
    play,
    playIO,
    module Reactive.Banana.Gloss.Combinators,
    module Reactive.Banana.Gloss.MomentGloss,
    module Graphics.Gloss.Interface.IO.Game,
    module Data.Array.Accelerate.Data.Colour.RGBA,
    module Graphics.Gloss.Accelerate.Data.Point,
    compile,
    actuate,
    pause,
    ) where

import Control.Concurrent.MVar
import Data.Array.Accelerate (Acc, Array, Arrays, DIM2)
import Data.Array.Accelerate.Data.Colour.RGBA
import Data.IORef
import Data.Set
import Graphics.Gloss.Accelerate.Data.Point
import Graphics.Gloss.Accelerate.Raster.Array
import Graphics.Gloss.Accelerate.Render
import Graphics.Gloss.Interface.IO.Game hiding (Event, playIO, Point)
import qualified Graphics.Gloss.Interface.IO.Game as Gloss hiding (Point)
import Reactive.Banana.Frameworks
import Reactive.Banana.Gloss.Combinators
import Reactive.Banana.Gloss.MomentGloss

play :: Arrays world =>
       Render
    -> Display
    -> (Int, Int)
    -> Int
    -> (Acc world -> Acc (Array DIM2 Colour))
    -> MomentGloss (Behavior world)
    -> IO ()
play render display resolution framerate draw react = playIO render display resolution framerate draw $ liftMomentGloss react

playIO :: Arrays world =>
       Render
    -> Display
    -> (Int, Int)
    -> Int
    -> (Acc world -> Acc (Array DIM2 Colour))
    -> MomentGlossIO (Behavior world)
    -> IO ()
playIO render display resolution framerate draw react = do
    actVar <- newEmptyMVar
    let network = playIO' render display resolution framerate draw ((,) () <$> react)  >>= liftIO . putMVar actVar . snd
    en <- compile network
    act <- takeMVar actVar
    actuate en
    act

playIO' :: Arrays world =>
       Render
    -> Display
    -> (Int, Int)
    -> Int
    -> (Acc world -> Acc (Array DIM2 Colour))
    -> MomentGlossIO (a, Behavior world)
    -> MomentIO (a, IO ())
playIO' render display resolution framerate draw react = do
    (_event, pushEvent) <- newEvent
    (_tick, pushTick) <- newEvent
    (a, worldB) <- runMomentGlossIO react =<< createEnv _event _tick display
    world <- valueB worldB
    worldV <- liftIO $ newIORef world
    reactimate' =<< (changes $ writeIORef worldV <$> worldB)
    return (a, playArrayIOWith render display resolution framerate ()
        (\() -> readIORef worldV)
        draw
        (\e () -> pushEvent e)
        (\t () -> pushTick t)
        )
